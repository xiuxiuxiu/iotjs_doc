
## Welcome
*Written for v1.9.5, revision 2*

这里是[AnyPi](https://anypi.com/)的文档中心，此文档针对有一定JavaScript基础的开发人员。anypi-iotjs可在AnyPi开发板进行开发与调试。


### H/W Reference
- V.ZEN-DevKit V1.2 board

![](./_static/vzen.jpg)

---

## How to get started?
What You Need
- 1 × V.ZEN-DevKit V1.2 board
- 1 × USB A / micro USB B cable
- 1 × PC loaded with Windows, Linux or Mac OS

我们可以通过三种方式登录开发板，登录开发板之后按照API写js文件，在js文件同目录执行以下命令运行程序，```CTRL+C```退出程序
```
iotjs xxx.js
```

### SSH登录
1. 使V.ZEN连网。V.ZEN会自动连接ssid:```anypi```、password:```12345678```的WiFi。或者，你可以长按SETUP按键5秒，让V.ZEN进入配网模式，下载[APP](https://fir.im/ionicvobot)，给VZEN配网
2. 登录路由器，查看在线设备，获得V.ZEN的IP
3. 保证PC与V.ZEN在局域网内
4. SSH登录，不同平台命令命令可能不一样。端口3333，密码:```123456```
```
ssh root@192.168.xx.xx 3333
```

### 串口登录
1. PC环境搭建略
2. 接线：GND<=>GDN，TX<=>RX，RX<=>TX
3. 参数```Baud Rate:115200```，```Data Bits:8```，```Stop Bits:1```，```Parity:None```，```Flow Control:None```
4. 接好线、连接上COM之后开机
5. 直到打印```Welcome to Vobot```且不再打印日志，按回车并输入用户名```root```和密码```123456```

### ADB登录
1. PC环境搭建略
2. 开机之后等待数秒（不会超过2分钟），PC输入```adb devices```看到设备```20080411```
3. PC输入```adb shell```连接ADB，出现```sh-4.4#```
4. 输入```source /etc/profile```，出现```root@VOBOT-XXXXXXXXXXXX />```

---

## Accelerometer
### face up
the document is being in progress.

---
## AI
### Baidu ASR
the document is being in progress.

### Baidu TTS
the document is being in progress.

---

## Button
<span id="button"></span>
我们可以通过IPC监听按键，一个按键事件包含两个信息：
- ```code```：按键键值
- ```status```：按键状态，分为按键的按下和抬起

根据硬件的不同，可监听到的键值也不同。按键事件的产生不一定来自于物理按键，也有可能来自于系统事件。以下为目前支持的所有键值:
- **[0] voice**
- **[1] mute**
- **[2] volume_up**
- **[3] volume_down**
- **[4] circle**
- **[5] proximity**
- **[6] direction**
- **[7] play_pause**
- **[8] mode**
- **[9] dc_input**
- **[10] shaking**
- **[11] operator_1**
- **[12] operator_2**
- **[13] brightness_up**
- **[14] brightness_down**
- **[15] select**
- **[16] snooze**
- **[17] power**
- **[18] setup**
- **[19] stop**
- **[20] sleep**

可监听到的按键状态：
- **[0] pressing_down**
- **[1] click_end**

<font color=Darkorange>eg. ↓↓↓</font>

- 监听按键事件：
```JavaScript
var ipc = require('ipc');

ipc.on('button', function(code, status) {
    console.log('Button:', code, '| Status:', status)
});
```

- 按下开发板的VOL-、VOL+、VOICE按键，可以看到如下输出，按一次按键会产生按下和抬起两个事件：
```
Button: volume_down | Status: pressing_down
Button: volume_down | Status: click_end
Button: volume_up | Status: pressing_down
Button: volume_up | Status: click_end
Button: voice | Status: pressing_down
Button: voice | Status: click_end
```

FAQ. *如何模拟一个按键事件？*

在开发过程中，我们往往需要模拟一个按键事件的发生，当物理按键暂时不支持、或者该事件暂时未发生时，我们可以通过执行命令模拟按键事件的发生。

在上面我们可以看到```volume_up```按键对应的数字为2，```pressing_down```状态对应的数字为0，```click_end```状态对应的数字为1。我们可以通过如下两个命令模拟voice按键的按下和抬起事件：

```
ubus send key '{"code":2, "status":0}'
ubus send key '{"code":2, "status":1}'
```

---

## Fs
我们通过Fs对文件进行读写操作。需要注意的是，js只能读写js所在目录下的文件，如果需要读写其他目录的文件，可以在js所在目录创建符号链接，让js可以访问该文件。

<font color=Darkorange>eg. ↓↓↓</font>

- 读文件。需先在js目录创建```read_test.txt```文件

```JavaScript
var fs = require('fs');
var file = 'read_test.txt';

fs.readFile(file, function (err, data) {
    if (err) {
        console.log(err);
    } else {
        console.log(data.toString());
    }
});
```

- 写文件。会在js同目录生成```write_test.txt```
```JavaScript
var fs = require('fs');
var file = 'write_test.txt';
var str = 'write file test!';

fs.writeFile(file, str, function (err) {
    if (err)
        console.log(err);
    else
        console.log(str);
});
```

---

## General
### signals
程序退出的几种方式

- 程序执行完毕自动退出
- 程序接收到```SIGINT```即CTRL+C信号退出
- 程序接收到```SIGTERM```即killall信号退出

在程序退出时，我们需要做退出的清理工作，以便程序正常退出。或者“留遗言”，以便开发者知道程序是何种原因退出。所以我们需要对信号进行监听。<font color=Darkorange>eg. ↓↓↓</font>

```JavaScript
var signals = require('signals');

// killall ...
signals.on('SIGTERM', ()=>{
    console.log('signal TERM, exit now');
    signals.stop();
});

// Ctrl + C
signals.on('SIGINT', ()=>{
    console.log('signal INT, exit now');
    signals.stop();
});

process.on('exit', function(code) {
    console.log('bye (' + code + ')');
});

```

### parse json
json的解析。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var user = {
    name: 'John',
    email: 'john@awesome.com',
    plan: 'Pro'
};

var userStr = JSON.stringify(user);

JSON.parse(userStr, function (key, value) {
    if (typeof value === 'string') {
        console.log(key, value);
        return value.toUpperCase();
    }
    return value;
});

console.log(userStr);
```

### timer
定时器。1秒和3秒的定时。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
console.log('timer start');

var loopTimeout = setTimeout(function() {
    console.log('1s timeout, bye!');
}, 1 * 1000);

var anotherTimeout = setTimeout(() => {
    console.log('3s timeout, bye!');
}, 3 * 1000);
```

---

## HTTPS
### http client
- 请求```www.baidu.com```，会获取到html返回并打印。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var http = require('http');
var options = {
    hostname: 'www.baidu.com',
    method: 'GET',
    port: 80,
    path: '/'
};

http.request(options, function (res) {
    receive(res, function (data) {
        console.log(data);
    });
}).end();

function receive(incoming, callback) {
    var data = '';

    incoming.on('data', function (chunk) {
        data += chunk;
    });

    incoming.on('end', function () {
       callback ? callback(data) : '';
    });
}
```

### https client
- 同上，支持https请求。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var https = require('https');
var options = {
    host: 'www.baidu.com',
    method: 'GET',
    port: 443,
    path: '/',
    ca: "-----BEGIN CERTIFICATE-----\n\
MIIE0zCCA7ugAwIBAgIQGNrRniZ96LtKIVjNzGs7SjANBgkqhkiG9w0BAQUFADCB\n\
yjELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL\n\
ExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJp\n\
U2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxW\n\
ZXJpU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0\n\
aG9yaXR5IC0gRzUwHhcNMDYxMTA4MDAwMDAwWhcNMzYwNzE2MjM1OTU5WjCByjEL\n\
MAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQLExZW\n\
ZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJpU2ln\n\
biwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxWZXJp\n\
U2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9y\n\
aXR5IC0gRzUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvJAgIKXo1\n\
nmAMqudLO07cfLw8RRy7K+D+KQL5VwijZIUVJ/XxrcgxiV0i6CqqpkKzj/i5Vbex\n\
t0uz/o9+B1fs70PbZmIVYc9gDaTY3vjgw2IIPVQT60nKWVSFJuUrjxuf6/WhkcIz\n\
SdhDY2pSS9KP6HBRTdGJaXvHcPaz3BJ023tdS1bTlr8Vd6Gw9KIl8q8ckmcY5fQG\n\
BO+QueQA5N06tRn/Arr0PO7gi+s3i+z016zy9vA9r911kTMZHRxAy3QkGSGT2RT+\n\
rCpSx4/VBEnkjWNHiDxpg8v+R70rfk/Fla4OndTRQ8Bnc+MUCH7lP59zuDMKz10/\n\
NIeWiu5T6CUVAgMBAAGjgbIwga8wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8E\n\
BAMCAQYwbQYIKwYBBQUHAQwEYTBfoV2gWzBZMFcwVRYJaW1hZ2UvZ2lmMCEwHzAH\n\
BgUrDgMCGgQUj+XTGoasjY5rw8+AatRIGCx7GS4wJRYjaHR0cDovL2xvZ28udmVy\n\
aXNpZ24uY29tL3ZzbG9nby5naWYwHQYDVR0OBBYEFH/TZafC3ey78DAJ80M5+gKv\n\
MzEzMA0GCSqGSIb3DQEBBQUAA4IBAQCTJEowX2LP2BqYLz3q3JktvXf2pXkiOOzE\n\
p6B4Eq1iDkVwZMXnl2YtmAl+X6/WzChl8gGqCBpH3vn5fJJaCGkgDdk+bW48DW7Y\n\
5gaRQBi5+MHt39tBquCWIMnNZBU4gcmU7qKEKQsTb47bDN0lAtukixlE0kF6BWlK\n\
WE9gyn6CagsCqiUXObXbf+eEZSqVir2G3l6BFoMtEMze/aiCKm0oHw0LxOXnGiYZ\n\
4fQRbxC1lfznQgUy286dUV4otp6F01vvpX1FQHKOtw5rDgb7MzVIcbidJ4vEZV8N\n\
hnacRHr2lVz2XTIIM6RUthg/aFzyQkqFOFSDX9HoLPKsEdao7WNq\n\
-----END CERTIFICATE-----\n"
};

https.request(options, function (res) {
    receive(res, function (data) {
        console.log(data);
    });
}).end();

function receive(incoming, callback) {
    var data = '';

    incoming.on('data', function (chunk) {
        data += chunk;
    });

    incoming.on('end', function () {
        callback ? callback(data) : '';
    });
}
```

### http server
- 开启http server，端口```2222```。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var http = require('http');
var self;

function HttpServer() {
    self = this;
    this.server;
    self.start();
}

HttpServer.prototype.start = () => {
    //req请求信息, res返回信息
    console.log('httpServer start ')
    self.server = http.createServer((request, response) => {
        var requestUrl = request.url;
        var reqKes = Object.keys(request);
        var requestMethod = request.method;
        var readable = request.readable;
        var headers = request.headers;
        console.log('requestMethod', requestMethod, 'readable', readable, 'headers', headers)
        console.log('request reqKes', JSON.stringify(reqKes))
    }).listen(2222);
}

HttpServer.prototype.stop = () => {
    if (self.server) {
        console.log('HttpServer stop')
        self.server.close();
    } else {
        console.error('HttpServer stop error')
    }
}

module.exports = new HttpServer();
```
执行程序后，在与开发板处于局域网的PC的浏览器中请求机器ip加端口就可以看到打印
```
http://192.168.xx.xxx:2222
```

### https server
the document is being in progress.

---

## I2C
the document is being in progress.

---

## IFTTT
the document is being in progress.

---

## IPC
FAQ. *What's IPC?*

Interprocess communication (IPC) is a set of programming interfaces that allow a programmer to coordinate activities among different program processes that can run concurrently in an operating system. This allows a program to handle many user requests at the same time. Since even a single user request may result in multiple processes running in the operating system on the user's behalf, the processes need to communicate with each other. The IPC interfaces make this possible. Each IPC method has its own advantages and limitations so it is not unusual for a single program to use all of the IPC methods.

FAQ. *What's RPC?*

Remote procedure call (RPC) is an Inter-process communication technology that allows a computer program to cause a subroutine or procedure to execute in another address space (commonly on another computer on a shared network) without the programmer explicitly coding the details for this remote interaction.

So, RPC is just one kind of IPC.

这不重要，项目开发者并不需要理解进程间通讯的原理，只需要知道我们通过这种方式传递信息，并且名字叫IPC。项目开发者看到以下示例**不需要过于纠结**，我们会根据项目开发者的需求，直接给对应代码，给出以下示例只是为了让项目开发者有个大概的了解。

---

### ipc call ctrl
**ipc call ctrl**的主要作用是发消息给其他进程。一条ipc call ctrl需要包含三个参数
- **ipc client name**
- **ctrl ID**
- **message**

FAQ. *如何获取 ipc client name？*

通过执行以下命令，可以获取当前在运行中的ipc client
```
ubus list
```

FAQ. *如何获取 ctrl ID？*

通过执行以下命令，可以获取所有预设的ctrl ID，每个ctrl ID都对应一个事件，这些ctrl ID并不是固定不变的，他会随着版本一同升级。根据硬件的不同，支持的项也不同，项目开发者并不需要知道每一项的作用，我们会根据开发者的需求，另做更易于项目开发者理解的解释，这里不再对每一项做解释。
```
cat /var/run/data.all_ctrls

>>>>>

enable_audio,0
disable_audio,1
avs_do_capture,2
input_source_alsa,3
input_source_file,4
input_source_pipe,5
run_demo_display,6
run_test_display,7
breathing_display,8
datetime_update,9
avs_do_play,10
avs_do_pause,11
avs_do_play_previous,12
avs_do_play_next,13
polly_answer,14
controller_binded,15
controller_unbinded,16
avs_mute,17
avs_unmute,18
refresh_display,19
display_buffer,20
display_string,21
display_clear,22
display_vumeter,23
display_playing,24
display_enqueue,25
game_mode,26
play_alert_uri,27
play_alert_assets,28
play_media_uri,29
play_media_assets,30
stop_media,31
get_most_recent_alert,32
dismiss_most_recent_alert,33
notify_host_status,34
notify_host_go_standby,35
notify_host_go_normal,36
notify_host_boot_ok,37
notify_host_go_shutdown,38
notify_host_go_reboot,39
get_mcu_version,40
get_is_batttery_charging,41
get_battery_voltage,42
get_is_dc_in,43
set_rgb_display_mode,44
set_matrix_display,45
set_go_bootloader,46
set_wakeup_time,47
set_wakeup_time_cancel,48
get_work_mode,49
set_work_mode,50
get_matrix_brightness,51
set_matrix_brightness,52
get_usb_exists,53
set_mcu_time,54
set_kids_display,55
set_kids_brightness,56
set_kids_restricted_mode,57
get_bt_connect_status,58
get_bt_is_playing,59
set_bt_previous,60
set_bt_next,61
set_bt_volume_up,62
set_bt_volume_down,63
set_bt_play_pause,64
get_bt_volume,65
set_bt_volume,66
set_ring_display,67
set_sun_display,68
set_amp_ctrl,69
set_led_projector_ctrl,70
notify_workmode_changed,71
notify_is_battery_charging,72
notify_battery_voltage_get,73
notify_dc_in_out,74
notify_audio_source_changed,75
notify_is_bt_connected,76
notify_is_bt_playing,77
notify_volume_sync,78
notify_mcu_time_get,79
controller_wifi_connected,80
pcm_reload,81
pcm_pause,82
pcm_resume,83
notify_setup_sound_rcvd,84
notify_setup_ble_scan,85
notify_setup_ble_connect,86
ping,87
set_audio_output,88
notify_iosdev_insert,89
notify_iosdev_remove,90
unknown,91
```

<font color=Darkorange>eg. ↓↓↓</font>
- 发消息给dpmgr进程(该进程负责屏幕的显示)，让dpmgr执行清屏和显示"hello"字符串两个动作
```JavaScript
var ipc = require('ipc');
var c = require('vconstants');

// ipc is a public global object, no need to open
function callTest() {
    ipc.ctrl('dpmgr', c.display_clear, '', function(err, ret) {
        callSyncTest();
    });
}

function callSyncTest() {
    ipc.ctrlSync('dpmgr', c.display_string, 'hello');
}

callTest();
```

- 以上代码等同于以下两条命令。我们可以看到，在js代码中的ctrl ID用的是名字，而shell命令中用的是ctrl ID对应的数字
```
// 清屏幕
ubus call dpmgr ctrl '{"id":22,"msg":""}'
// 显示字符
ubus call dpmgr ctrl '{"id":21,"msg":"hello"}'
```

---

### ipc call ctrl handler
**ipc call ctrl handler**的主要作用是接收其他进程发给自己的消息。一条消息带有两个信息：
- **ctrl ID**
- **message**

<font color=Darkorange>eg. ↓↓↓</font>
- 启用simpleCtrl的监听，起名为myapp，并监听其他进程的SimpleCtrl调用
```JavaScript
var ipc = require('ipc');
ipc.createCallHandler('myapp');

ipc.on('ctrl', function(id, msg) {
    console.log('Received simple control:', id, "msg:", msg);
    if (id === 'unknown') {
        try {
            var msgJson = JSON.parse(msg);
            console.log(msgJson.hello);
        } catch (err) {
            console.log('error:', err);
            throw err;
        }
    }
});
```

- 在另一个窗口执行以下命令，即可看到打印信息
```
ubus call myapp ctrl '{"id":999, "msg":"{\"hello\":\"world\"}"}'
```

---

### ipc button
[ipc button 详解](#button)

---

### ipc system state
**ipc system state**的主要作用是监听系统消息，一条消息包含两个信息
- **state**
- **trigger**

已有的state
- **idle_no_network**
- **idle_wifi_connected**
- **idle_internet_connected**
- **ap_passcode_input**
- **ap_connecting**
- **ap_connected**
- **ap_connect_failed**
- **fw_checking_update**
- **fw_downloading**
- **fw_prepared**
- **fw_updating**
- **sys_critical_shutdown**
- **sys_scheduled_reboot**
- **sys_shutdown_canceled**

已有的trigger
- **none**
- **wifi_ok**
- **wifi_fail**
- **internet_ok**
- **internet_fail**
- **wificonf_start**
- **wificonf_cancel**
- **wificonf_try_connect**
- **wificonf_connect_success**
- **wificonf_connect_fail**
- **wificonf_phone_connect**
- **fw_check**
- **fw_not_found**
- **fw_found**
- **fw_download_success**
- **fw_download_fail**
- **fw_update_go**
- **fw_update_cancel**
- **fw_completed**
- **config_bind_success**
- **config_bind_fail**
- **config_auth_success**
- **config_auth_fail**
- **inval**

根据硬件的不同，支持的项也不同，项目开发者并不需要知道每一项的作用，我们会根据开发者的需求，另做更易于项目开发者理解的解释，这里不再对每一项做解释。

<font color=Darkorange>eg. ↓↓↓</font>
- 开启ipc system state的监听，可以监听到来自系统的消息
```JavaScript
var ipc = require('ipc');

ipc.on('systemState', function(state, trigger) {
    console.log('SYSTEM state:', state, "by trigger:", trigger);
});
```

---

### ipc voice service
the document is being in progress.

### rpc publish to remote
the document is being in progress.

### rpc remote simulation call
the document is being in progress.

---

## Js Lang
the document is being in progress.

---

## LED Matrix
### scroll
the document is being in progress.

### loop
the document is being in progress.

### image show
the document is being in progress.

---

## Machine
### device id
获取设备的serial number。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var v = require('vobot');

console.log(v.getSerialNumber());
```

---

## MQTT
the document is being in progress.

---

## Music
### esay play content
简单的播放一首歌。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var m = require('music');

// volume range: 0 ~ 100
m.setVolume(50);
m.play("http://somebody.oss-cn-shenzhen.aliyuncs.com/sounds/nanshannan.mp3");
```

### play content
根据配置播放歌曲。一条完整的歌曲配置信息如下：
```
{
    "assetPlayOrder": [
      "9999f3bf-978d-46d5-a9dd-f2b9d31f50d6"
    ],
    "assets": [
      {
        "assetId": "9999f3bf-978d-46d5-a9dd-f2b9d31f50d6",
        "name": "XXX",
        "type": "BUILT_IN",
        "url": "media/sleep/thunder.mp3"
      }
    ],
    "backgroundAlertAsset": "",
    "fadeIn": 15,
    "loopCount": 0,
    "loopPauseInMilliSeconds": 0,
    "token": "AUTOMATION-abcdefg",
    "type": "SOUND",
    "volume": 60
}
```
有些信息并不重要，只需关注以下信息：
- **type**：
  - 内置音乐```BUILT_IN```
  - 网络音乐```LINK```
- **url**：
  - 当type为```BUILT_IN```时，url为歌曲与```/opt```的相对路径
  - 当type为```LINK```时，url为歌曲链接
- **fadeIn**：渐进效果，单位(秒)
- **loopCount**：循环播放次数(暂不支持)，为0时代表永不停止
- **loopPauseInMilliSeconds**：多少毫秒之后停止播放(暂不支持)，为0时代表永不停止
- **type**：对于普通开发者来说，只需用```SOUND```
  - 闹铃```ALARM```
  - 声音```SOUND```
- **volume**：音量，0-100

<font color=Darkorange>eg. ↓↓↓</font>
- 播放歌曲，通过ipc ctrl，把歌曲配置信息发给appavs进程即可
```JavaScript
var ipc = require('ipc');

var pstr = JSON.stringify({
    "assetPlayOrder": [
      "9999f3bf-978d-46d5-a9dd-f2b9d31f50d6"
    ],
    "assets": [
      {
        "assetId": "9999f3bf-978d-46d5-a9dd-f2b9d31f50d6",
        "name": "XXX",
        "type": "BUILT_IN",
        "url": "media/sleep/thunder.mp3"
      }
    ],
    "duration": 0
    "backgroundAlertAsset": "",
    "fadeIn": 15,
    "loopCount": 0,
    "loopPauseInMilliSeconds": 0,
    "token": "AUTOMATION-abcdefg",
    "type": "SOUND",
    "volume": 60
});

ipc.ctrl('appavs', c.play_media_assets, pstr, function(err, ret) {
    if (err)
        console.log(err);
    else
        console.log(ret);
});
```

### stop play
停止播放歌曲，模拟stop按键的按下动作即可。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
var ipc = require('ipc');
var Button = {
    STOP: 'stop'
};

ipc.keySimulation(Button.STOP, 'pressing_down');
```

---

## Peripheral
the document is being in progress.

---

## Recorder
```startRecorder```录音的开启需要两个参数
- ```interval```：录音的时间间隔，单位ms
- ```plughw```：硬件选择
  - 单麦硬件需要使用```plughw:0,0```
  - 双麦硬件需要使用```plughw:1,0```

### esay recorder
录音结果直接打印。<font color=Darkorange>eg. ↓↓↓</font>
```JavaScript
recorder = require('recorder');

// capture microphone every 50ms
recorder.startRecorder(50, 'plughw:1,0');

recorder.onCapture(function(len, data) {
    console.log('Byte length=' + len);
    for (var i = 0; i < len; i+=2) {
        var num = data.readUInt16LE(i);
        console.print(('0000' + (num < 0 ? (0xFFFF + num + 1) : num).toString(16)).substr(-4) + ' ');
    }
    console.log();
});

setTimeout(function() {
    recorder.stopRecorder();
    console.log('test completed');
}, 5000);

```

### recorder to file
录音5秒，结果输出为```var/captured.pcm```。<font color=Darkorange>eg. ↓↓↓</font>

```JavaScript
recorder = require('recorder');
var util = require('util');
var fs = require('fs');
var streamBuffers = require('stream').Readable;

var myReadableStreamBuffer = new streamBuffers();

var dstFilePath = 'var/captured.pcm';

// capture microphone every 50ms
recorder.startRecorder(50, 'plughw:1,0'); // 'default'

fs.appendFileSync = function(path, data) {
    var fd;
    var len;
    var bytesWritten;

    var writeSync = function() {
        var tryN = (len - bytesWritten) >= 1024 ? 1023 : (len - bytesWritten);
        var n = fs.writeSync(fd, data, bytesWritten, tryN, bytesWritten);
        afterWriteSync(n);
    };

    var afterWriteSync = function(n) {
        if (n <= 0 || bytesWritten + n == len) {
            // End of buffer
            fs.closeSync(fd);
        } else {
        // continue writing
        bytesWritten += n;
        writeSync();
        }
    };

    fd = fs.openSync(path, 'a', 438);
    len = data.length;
    bytesWritten = 0;
    writeSync();
};

var totalLen = 0;
recorder.onCapture(function(len, data){
    totalLen += len;
    console.log('Byte length=' + len, 'Total=' + totalLen);
    myReadableStreamBuffer.push(data);
});

myReadableStreamBuffer.on('data', function (data) {
    fs.appendFileSync(dstFilePath, data);
});

myReadableStreamBuffer.on('error', function(err) {
    console.log(err);
});

myReadableStreamBuffer.on('end', function () {
    console.log('all data wrote');
});

setTimeout(function(){
    recorder.stopRecorder();
    console.log('test completed');
}, 5000);
```

可用winscp软件或adb pull将pcm文件传到PC，或用wget命令将pcm文件传到服务器。

pcm参数如下：
- **signed 16bit PCM**
- **little endian**
- **单声道**
- **采样率16000**

---

## Translator
the document is being in progress.

---

## Websocket
the document is being in progress.
